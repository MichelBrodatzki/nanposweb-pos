from lcd import POS_LCD
from evdev import InputDevice
from hid_handler import HIDInputHandler
from dotenv import load_dotenv
import os
import threading
import time
import requests
import atexit
import json
import pprint

lcd = None
lcd_thread = None

if __name__ == '__main__':
	load_dotenv()

	if os.environ.get('ENABLE_I2C_LCD', False):
		lcd_height = os.environ.get('I2C_LCD_HEIGHT')
		lcd_width = os.environ.get('I2C_LCD_WIDTH')

		if lcd_height is None or lcd_width is None:
			print("I2C_LCD_HEIGHT and I2C_LCD_WIDTH have to be set if ENABLE_I2C_LCD is true")
			exit(1)

		lcd = POS_LCD(int(os.environ.get('I2C_LCD_ADDRESS', 0x27), 16), int(lcd_width), int(lcd_height))
		lcd_thread = lcd.get_thread().start()

	print("Loading nanpos-web")
	nanposweb_url = os.environ.get('NANPOSWEB_URL', None)
	if nanposweb_url is None:
		print ("NANPOSWEB_URL has to be set!")
		exit(2)

	nanposweb_url.removesuffix("/")

	if lcd:	
		lcd.info('Loading ...')

	try:
		r = requests.options(f"{nanposweb_url}/pos")
		if r.status_code != 200:
			print(f"Failed to connect to nanposweb ({r.status_code})")

			if lcd:
				lcd.error(f'Error\r\n{r.status_code}')
				exit(2)
	except requests.exceptions.ConnectionError:
		print("Failed to connect to nanposweb. Check if URL is correct!")
		if lcd:
			lcd.error(f'Error\r\n-2')
		exit(2)

	user = None
	user_timeout = None
	product = None
	product_timeout = None

	def reset_user():
		global user, user_timeout
		user = None
		user_timeout = None
		if lcd:
			lcd.set_user(user)

	def reset_product():
		global product, product_timeout
		product = None
		product_timeout = None
		if lcd:
			lcd.set_product(product)

	if os.environ.get('ENABLE_BARCODE_SCANNER'):
		print ("Enabling barcode scanner ...")
		scanner = InputDevice(os.environ.get('BARCODE_SCANNER_INPUT_DEVICE'))
		hid = HIDInputHandler()
		scanner.grab()
		atexit.register(scanner.ungrab)
		def line_read_finished(line: str) -> None:
			global product_timeout, user_timeout, product, user, nanposweb_url

			product_r = requests.get(f"{nanposweb_url}/pos/product?ean={line}")

			if product_r.status_code == 200:
				if product_timeout is not None:
					product_timeout.cancel()
					product_timeout = None
				product_timeout = threading.Timer(int(os.environ.get('POS_TIMEOUT', 10)), reset_product)
				product_timeout.start()
				product = json.loads(product_r.text)
				if lcd:
					lcd.set_product(product)
			else:
				user_r = requests.get(f"{nanposweb_url}/pos/card?id={line}")

				if user_r.status_code == 200:
					if user_timeout is not None:
						user_timeout.cancel()
						user_timeout = None
					user_timeout = threading.Timer(int(os.environ.get('POS_TIMEOUT', 10)), reset_user)
					user_timeout.start()
					user = json.loads(user_r.text)
					if lcd:
						lcd.set_user(user)
				else:
					lcd.info("Unbekannter\r\nBarcode!")
			
			if product is not None and user is not None:
				if product_timeout is not None:
					product_timeout.cancel()
					product_timeout = None

				if user_timeout is not None:
					user_timeout.cancel()
					user_timeout = None
				pass

				transaction_request = requests.post(f"{nanposweb_url}/pos/transaction", data={'product_id': product['product']['id'], 'user_id': user['user']['id']})

				if transaction_request.status_code == 200:
					transaction = json.loads(transaction_request.text)
					if lcd:
						lcd.info(f"Produkt gekauft!\r\n{int(user['user']['balance']) / 100:.2f}\x00 \x03 {int(transaction['user']['balance']['new'])/100:.2f}\x00")
				else:
					error_text = ""
					
					error = json.loads(transaction_request.text)
					if "message" in error:
						error_text = error['message']

					if lcd:
						lcd.info(f"Fehler!\r\n{error_text}")

				reset_user()
				reset_product()


		hid.on_line_end(line_read_finished)

		def hid_poller_thread_func():
			for event in scanner.read_loop():
				hid.handle(event)
	
		barcode_scanner_thread = threading.Thread(target=hid_poller_thread_func).start()

	while (True):
		time.sleep(1)