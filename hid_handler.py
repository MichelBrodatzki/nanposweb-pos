from evdev import InputEvent, ecodes, categorize
from typing import Callable

class HIDInputHandler:
    def __init__(self, lf = True):
        self.current_line = ""
        self.lf = lf
        self.modifier = {
            "shift": False,
            "ctrl": False,
            "alt": False,
            "meta": False
        }

    def on_line_end(self, callback: Callable[[str], None]):
        self.callback = callback

    def handle(self, evt: InputEvent):
        if evt.type == ecodes.EV_KEY:
            if evt.code in [ecodes.KEY_LEFTSHIFT, ecodes.KEY_RIGHTSHIFT]:
                if evt.value == 1:
                    self.modifier["shift"] = True
                else:
                    self.modifier["shift"] = False
            elif evt.code in [ecodes.KEY_LEFTCTRL, ecodes.KEY_RIGHTCTRL]:
                if evt.value == 1:
                    self.modifier["ctrl"] = True
                else:
                    self.modifier["ctrl"] = False
            elif evt.code in [ecodes.KEY_LEFTALT, ecodes.KEY_RIGHTALT]:
                if evt.value == 1:
                    self.modifier["alt"] = True
                else:
                    self.modifier["alt"] = False
            elif evt.code in [ecodes.KEY_LEFTMETA, ecodes.KEY_RIGHTMETA]:
                if evt.value == 1:
                    self.modifier["meta"] = True
                else:
                    self.modifier["meta"] = False
            else:
                if evt.value == 1:
                    cat_evt = categorize(evt)
                    if evt.code == ecodes.KEY_J and self.modifier["ctrl"] and self.modifier["shift"]:
                        if self.lf:
                            self.callback(self.current_line)
                            self.current_line = ""
                        return

                    if evt.code == ecodes.KEY_ENTER:
                        if not self.lf:
                            self.callback(self.current_line)
                            self.current_line = ""
                        return

                    key = cat_evt.keycode[4:]

                    replacements = {
                        "MINUS": "-",
                        "ESCAPE": "",
                        "ESC": "",
                        "EQUAL": "=",
                        "LEFTBRACE": "{",
                        "RIGHTBRACE": "}",
                        "SEMICOLON": ";",
                        "APOSTROPHE": "'",
                        "GRAVE": "`",
                        "BACKSLASH": "\\",
                        "COMMA": ",",
                        "DOT": ".",
                        "SLASH": "/",
                        "KPASTERISK": "*",
                        "SPACE": " ",
                        "KP7": "7", 
                        "KP8": "8",
                        "KP9": "9",
                        "KPMINUS": "-",
                        "KP4": "4",
                        "KP5": "5",
                        "KP6": "6",
                        "KPPLUS": "+",
                        "KP1": "1",
                        "KP2": "2",
                        "KP3": "3",
                        "KP0": "0",
                        "KPDOT": "."
                    }

                    if key in replacements:
                        key = replacements[key]
                    
                    if len(key) != 1:
                        print(f"ERROR: Cannot find replacement for key '{key}'!")
                    else:
                        if self.modifier["shift"]:
                            self.current_line += key
                        else:
                            self.current_line += key.lower()
