from RPLCD.i2c import CharLCD
import os
import time
import threading
import math

class LCD_String:
    def __init__(self, text: str, width: int, right_aligned: bool = False):
        self.text = text
        self.width = width
        self.right_aligned = right_aligned
        self.window = 0
    
    def move(self):
        self.window = self.window + 1
        if self.window + self.width > len(self.text):
            self.window = 0

    def get(self):
        if len(self.text) > self.width:
            return self.text[self.window:self.window+self.width]
        else:
            if self.right_aligned:
                return self.text.rjust(self.width)
            else:
                return self.text.ljust(self.width)
    
class POS_LCD:
    def __init__(self, address: int, width: int, height: int):
        self.lcd = CharLCD('PCF8574', address)

        euroChar = (
            0b00110,
            0b01001,
            0b11100,
            0b01000,
            0b11100,
            0b01001,
            0b00110,
            0b00000
        )
        self.lcd.create_char(0, euroChar)

        userChar = (
            0b00000,
            0b00110,
            0b00110,
            0b00000,
            0b00110,
            0b01111,
            0b01111,
            0b00000
        )
        self.lcd.create_char(1, userChar)

        cartChar = (
            0b00000,
            0b00000,
            0b11000,
            0b00111,
            0b00111,
            0b00000,
            0b00101,
            0b00000
        )
        self.lcd.create_char(2, cartChar)

        arrowChar = (
            0b00000,
            0b00000,
            0b00100,
            0b00010,
            0b11111,
            0b00010,
            0b00100,
            0b00000
        )
        self.lcd.create_char(3, arrowChar)

        self.width = width
        self.height = height
        self.product = None
        self.user = None
        self.is_error = False
        self.is_info = False

    def print_lines(self, lines: list):
        if len(lines) > self.height:
            lines = lines[0:self.height]
        
        line_strs = [line.get() for line in lines]
        self.lcd.write_string("\r\n".join(line_strs) + "\r\n")

        for clear_lines in range(self.height - len(lines)):
            self.lcd.write_string("\r\n".rjust(self.width))

        for line in lines:
            line.move()

    def run(self):
        while True:
            self.lcd.cursor_pos = (0, 0)
            if self.is_error:
                    self.print_lines(self.error_text)
            elif self.is_info:
                if time.time() - self.info_timer > int(os.environ.get('POS_INFO_TIMEOUT', 5)):
                    self.is_info = False
                else:
                    self.print_lines(self.info_text)
            else:
                user_text = "-"
                product_text = "-"

                if self.user is not None:
                    user_text = f"{self.user['name'].get()} {self.user['balance'].get()}"
                    self.user['name'].move()
                    self.user['balance'].move()

                if self.product is not None:
                    product_text = f"{self.product['name'].get()} {self.product['price'].get()}"
                    self.product['name'].move()
                    self.product['price'].move()

                self.lcd.cursor_pos = (0, 0)
                self.lcd.write_string(f"\x01 {user_text.ljust(self.width-1)}\r\n\x02 {product_text.ljust(self.width-1)}")


            time.sleep(1)            

    def get_thread(self):
        return threading.Thread(target=self.run)

    def info(self, text: str):
        self.info_text = [LCD_String(line, self.width) for line in text.split("\r\n")]
        self.is_info = True
        self.info_timer = time.time()

    def error(self, text: str):
        self.error_text = [LCD_String(line, self.width) for line in text.split("\r\n")]
        self.is_error = True
    
    def set_product(self, product: dict):
        if product is None:
            self.product = None
        else:
            self.product = {'name': None, 'price': 0}
            price_text = f"{product['product']['price'] / 100:.2f}\x00"
            self.product['name'] = LCD_String(product['product']['name'], self.width - len(price_text) - 3)
            self.product['price'] = LCD_String(price_text, len(price_text), True)

    def set_user(self, user: dict):
        if user is None:
            self.user = None
        else:
            self.user = {'name': None, 'balance': 0}
            balance_text = f"{user['user']['balance'] / 100:.2f}\x00"
            self.user['name'] = LCD_String(user['user']['name'], self.width - len(balance_text) - 3)
            self.user['balance'] = LCD_String(balance_text, len(balance_text), True)